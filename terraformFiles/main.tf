
provider "google" {
  credentials = file("project-task-fixably-4da83815e6e2.json")
  project = var.project
  region = var.region
  zone = var.location
}
resource "google_container_cluster" "default" {
  name        = var.name
  project     = var.project
  description = var.description
  location    = var.location

  remove_default_node_pool = true
  initial_node_count       = var.initial_node_count

  
}

resource "google_container_node_pool" "default" {
  name       = "${var.name}-node-pool"
  project    = var.project
  location   = var.location
  cluster    = google_container_cluster.default.name
  node_count = 3

  node_config {
    preemptible  = true
    machine_type = var.machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
