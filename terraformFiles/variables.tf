variable "name" {
  default = "cluster-fixably"
}

variable "project" {
  default = "project-task-fixably"
}

variable "description" {
  default = "Task Project for Fixably"
}

variable "region" {
  default = "europe-north1"
}

variable "location" {
  default = "europe-north1-a"
}

variable "initial_node_count" {
  default = 3
}

variable "machine_type" {
  default = "e2-micro"
}
